<?php
namespace App;

use PDO;
use PDOException;


class Database{

    public $host="localhost";
    public $dbname="atomic_project_b37";
    public $user="root";
    public $pass="";

    public function __construct()
    {
        try {


            $DBH = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->user, $this->pass);
            $DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            echo"Connected";


        }
        catch(PDOException $e){
            echo "<br>";
            echo "I'm sorry. I'm afraid I can't do that.";
            file_put_contents('PDOErrors.txt', $e->getMessage(), FILE_APPEND);

        }

    }
}

